## Instalasi Proyek
<ul>
    <li>1. Database proyek terletak pada folder database/inventory</li>
    <li>2. Endpoint
        <ul>
            <li>1. GET : inventory/public/item (menampilkan list data item)</li>
            <li>2. POST : inventory/public/item (menambah data item) <br>
                parameter : 
                {
                    "name": "Mesin Cuci",
                    "price": "100000",
                    "stock": 0
                }
            </li>
            <li>3. PUT : inventory/public/item/{id} (mengubah data item)<br>
                parameter : 
                {
                    "id" : 1,
                    "name": "Mesin Cuci",
                    "price": "100000"
                }
            </li>
            <li>4. DELETE : inventory/public/item/{id} (menghapus data item)<br></li>
            <li>5. POST : inventory/public/item/purchase (menambah stok data item)<br>
                parameter : 
                {
                    "id" : 1,
                    "purchase": 20,
                    "secret_number": "5891",
                    "input_number": "1895"
                }
            </li>
        </ul>
    </li>
</ul>