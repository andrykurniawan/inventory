<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/item', 'Inventory@store');
Route::get('/item', 'Inventory@index');
Route::get('/item/{id}', 'Inventory@show');
Route::put('/item', 'Inventory@update');
Route::delete('/item/{id}', 'Inventory@destroy');
Route::post('/item/purchase', 'Inventory@purchase');
