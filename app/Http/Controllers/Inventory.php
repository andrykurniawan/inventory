<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Illuminate\Support\Facades\Validator;

class Inventory extends Controller
{
    public function index(){
        $items = Item::latest()->get();
        return response([
            'alert' => true,
            'msg' => 'Berhasil mendapatkan data',
            'data' => $items
        ], 200);
    }
    public function store(Request $request){
        $validation = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'stock' => 'required'
        ],
            [
            'name.required' => 'Masukan parameter name',
            'price.required' => 'Masukan parameter price',
            'stock.required' => 'Masukan parameter stock'
            ]
        );

        if($validation->fails()){
            return response()->json([
                'alert' => false,
                'msg' => 'Silahkan mengisi parameter yang dibutuhkan',
                'data' => $validation->errors()
            ], 401);
        }else{
            $items = Item::create([
                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'stock' => $request->input('stock')
            ]);
            if($items){
                return response()->json([
                    'alert' => true,
                    'msg' => 'Item berhasil disimpan'
                ], 200);
            }else{
                return response()->json([
                    'alert' => false,
                    'msg' => 'Item gagal disimpan'
                ], 401);
            }
        }
    }

    public function show($id)
    {
        $item = Item::whereId($id)->first();


        if ($item) {
            return response()->json([
                'alert' => true,
                'msg' => 'Berhasil mendapatkan data',
                'data'    => $item
            ], 200);
        } else {
            return response()->json([
                'alert' => false,
                'msg' => 'Data tidak ditemukan'
            ], 401);
        }
    }
    public function update(Request $request)
    {
            $data = array();
            if($request->input('name')){
                $data += array(
                    'name' => $request->input('name')
                );
            }
            if($request->input('price')){
                $data += array(
                    'price' => $request->input('price')
                );
            }
            if(count($data) == 0){
                return response()->json([
                    'alert' => false,
                    'msg' => 'Masukan parameter yang dibutuhkan',
                ], 401);
            }

            $item = Item::whereId($request->input('id'))->update($data);

            if ($item) {
                return response()->json([
                    'alert' => true,
                    'msg' => 'Item Berhasil Diupdate',
                ], 200);
            } else {
                return response()->json([
                    'alert' => false,
                    'msg' => 'Item Gagal Diupdate',
                ], 401);
            }
    }

    public function destroy($id)
    {
        $item = Item::find($id);
        if($item){
            $item->delete();
        }else{
            return response()->json([
                'alert' => false,
                'msg' => 'Item Tidak Ditemukan',
            ], 401); 
        }

        if ($item) {
            return response()->json([
                'alert' => true,
                'msg' => 'Item Berhasil Dihapus',
            ], 200);
        } else {
            return response()->json([
                'alert' => false,
                'msg' => 'Item Gagal Dihapus',
            ], 401);
        }

    }

    public function purchase(Request $request){
        $item = Item::find($request->input('id'));
        if(!$item){
            return response()->json([
                'alert' => false,
                'msg' => 'Item Tidak Ditemukan',
            ], 401); 
        }
        $validation = Validator::make($request->all(),[
            'purchase' => 'required'
        ],
            [
            'purchase.required' => 'Masukan parameter purchase'
            ]
        );

        if($validation->fails()){
            return response()->json([
                'alert' => false,
                'msg' => 'Mohon isi parameter purchase',
                'data' => $validation->errors()
            ], 401);
        }else{
            $item = Item::find($request->input('id'))->increment('stock',$request->input('purchase'));
            $json = array();
            if($request->input('secret_number') && $request->input('input_number')){
                $alhamdulillah = 0;
                $subhanallah = 0;
                $secret_number = str_split($request->input('secret_number'));
                $input_number = str_split($request->input('input_number'));
                $idx = 0;
                foreach($secret_number as $sn){
                    if(isset($input_number[$idx])){
                        if($sn === $input_number[$idx]){
                            $alhamdulillah += 1;
                        }else{
                            $subhanallah += 1;
                        }
                    }
                    $idx++;
                }
                $json += array(
                    'output' => $alhamdulillah." Alhamdulillah ".$subhanallah." Subhanallah"
                );
            }
            if ($item) {
                $json += array(
                    'alert' => true,
                    'msg' => 'Purchase Item Berhasil'
                );
                $kode = 200;
            } else {

                $json += array(
                    'alert' => false,
                    'msg' => 'Purchase Item Gagal'
                );
                $kode = 401;
            }

            return response()->json($json, $kode);
        }
    }
}
